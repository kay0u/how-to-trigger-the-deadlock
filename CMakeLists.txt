cmake_minimum_required(VERSION 3.5)

project(how-to-trigger-the-deadlock LANGUAGES CXX)

find_package(PkgConfig)

# You can change this path to where you compile/install your gstreamer
set(ENV{PKG_CONFIG_PATH} "/home/kayou/anaconda3/envs/gst/lib/pkgconfig/")
pkg_check_modules(GST REQUIRED gstreamer-1.0)

add_executable(trigger_the_deadlock main.cpp)
target_include_directories(trigger_the_deadlock PRIVATE ${GST_INCLUDE_DIRS})
target_link_libraries(trigger_the_deadlock PRIVATE ${GST_LIBRARIES} pthread)
