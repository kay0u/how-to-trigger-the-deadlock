#include <gst/gst.h>
#include <thread>

static GMainLoop *loop;

static gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer data);

class Pipeline {
public:
    Pipeline() : pipeline(gst_pipeline_new ("my_pipeline")),
                source(gst_element_factory_make ("v4l2src", "source")),
                jpegdec(gst_element_factory_make("jpegdec", "dec")),
                enc(gst_element_factory_make ("nvh265enc", "enc")),
                h265parse(gst_element_factory_make ("h265parse", "parse")),
                sink(gst_element_factory_make ("rtspclientsink", "sink"))
    {
        GstBus *bus = gst_pipeline_get_bus (GST_PIPELINE (pipeline));
        bus_watch_id = gst_bus_add_watch (bus, my_bus_callback, this);

        g_object_set(G_OBJECT (sink), "location", "rtsp://127.0.0.1:40000/test", NULL);
        gst_bin_add_many (GST_BIN (pipeline), source, jpegdec, enc, h265parse, sink, NULL);

        if (!gst_element_link_many (source, jpegdec, enc, h265parse, sink, NULL)) {
            g_warning ("Failed to link elements!");
        }
    }

    ~Pipeline()
    {
        gst_element_set_state(pipeline, GST_STATE_NULL);
        gst_object_unref(GST_OBJECT (pipeline));
        if (bus_watch_id)
            g_source_remove(bus_watch_id);
    }

    void setError() { error = true; }
    bool getError() { return error; }
    void setInProgress(bool v) { in_progress = v; }
    bool getInProgress() { return in_progress; }
    GstState getTargetState() { return target_state; }

    GstStateChangeReturn setState(GstState state)
    {
        target_state = state;
        return gst_element_set_state(pipeline, state);
    }

    GstStateChangeReturn start()
    {
        error = false;
        return setState(GST_STATE_PAUSED);
    }

    GstStateChangeReturn stop()
    {
        return setState(GST_STATE_NULL);
    }

    std::pair<GstState, GstState> getState(GstClockTime timeout = GST_CLOCK_TIME_NONE)
    {
        GstState state;
        GstState pending;
        gst_element_get_state(pipeline, &state, &pending, timeout);
        return std::make_pair(state, pending);
    }

private:
    GstElement *pipeline;
    GstElement *source, *jpegdec, *enc, *h265parse, *sink;
    guint bus_watch_id;
    GstState target_state{GST_STATE_NULL};

    bool in_progress{false};
    bool error{false};
};

static gboolean my_bus_callback (GstBus * bus, GstMessage * message, gpointer data)
{
    Pipeline *pipeline = static_cast<Pipeline *>(data);

    g_print("Got %s message\n", GST_MESSAGE_TYPE_NAME (message));

    switch (GST_MESSAGE_TYPE (message))
    {
        case GST_MESSAGE_ERROR:
        {
            GError *err;
            gchar *debug;

            gst_message_parse_error(message, &err, &debug);
            g_print("Error: %s\n", err->message);
            g_error_free(err);
            g_free(debug);

            pipeline->setError();
            break;
        }
        case GST_MESSAGE_PROGRESS:
        {
            GstProgressType type;
            gchar *code, *text;

            gst_message_parse_progress(message, &type, &code, &text);

            switch (type)
            {
                case GST_PROGRESS_TYPE_START:
                case GST_PROGRESS_TYPE_CONTINUE:
                    pipeline->setInProgress(true);
                    break;
                case GST_PROGRESS_TYPE_COMPLETE:
                case GST_PROGRESS_TYPE_CANCELED:
                case GST_PROGRESS_TYPE_ERROR:
                    pipeline->setInProgress(false);
                    break;
                default:
                    break;
            }
            g_free(code);
            g_free(text);

            if (!pipeline->getInProgress() && pipeline->getTargetState() == GST_STATE_PAUSED)
            {
                if (pipeline->setState(GST_STATE_PLAYING) == GST_STATE_CHANGE_FAILURE)
                {
                    pipeline->setError();
                    g_printerr("ERROR: Can't start pipeline");
                }
            }
            break;
        }
        case GST_MESSAGE_EOS:
            g_main_loop_quit(loop);
            break;
        default:
            break;
    }

    return TRUE;
}


int main(int argc, char *argv[])
{
    std::thread thread;

    // init
    gst_init (&argc, &argv);

    std::unique_ptr<Pipeline> pipeline = std::make_unique<Pipeline>();

    pipeline->start();

    bool run = true;
    auto auto_restart_lambda = [&]()
    {
        using namespace std::chrono;
        while (run)
        {
            std::this_thread::sleep_for(50ms);

            auto[state, pending] = pipeline->getState(10000000);
            if ((state != GST_STATE_PLAYING && state != GST_STATE_PAUSED && !pipeline->getInProgress()) || pipeline->getError())
            {
                pipeline = std::make_unique<Pipeline>();
                pipeline->start();
            }
        }
    };
    thread = std::thread(auto_restart_lambda);
    loop = g_main_loop_new (NULL, FALSE);
    g_main_loop_run (loop);

    g_main_loop_unref (loop);
    run = false;

    if (thread.joinable())
        thread.join();
}
